package calculator;

public class Calculator {

    private int value;

    //Constructor
    public Calculator() {
        value = 0;
    }

    //Add Integer
    public void addInteger(int number){
        value += number;
    }

    //Get current value
    public int getValue() {
        return value;
    }

    //Reset
    public void reset() {
        value = 0;
    }


}
