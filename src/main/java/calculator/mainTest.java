package calculator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class mainTest {

    //Test sum method of Main
    @Test
    void sum() {
        Calculator calculator = new Calculator();
        int[] numbers = {10, 7, 6};
        assertEquals(23, Main.sum(numbers, calculator));
    }

    //Test sum method of Main with negative integer
    @Test
    void sumNegative() {
        Calculator calculator = new Calculator();
        boolean thrown = false;
        int[] numbers = {10, 7, -6};
        try {
            Main.sum(numbers, calculator);
        } catch (IllegalArgumentException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }

}

