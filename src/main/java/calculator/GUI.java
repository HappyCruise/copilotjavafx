package calculator;

import javafx.application.Application;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


/**
 * A javaFX GUI for the calculator
 */
public class GUI extends Application {
    
    //Display a field for the user to enter numbers and a button to calculate the sum using Main.sum(), and a button to reset the calculator and a field to display the result
    @Override
    public void start(Stage stage) throws Exception {
        //Create a new calculator
        Calculator calculator = new Calculator();

        //Create a new grid pane
        GridPane gridPane = new GridPane();

        //Create a text field for the user to enter numbers
        TextField textField = new TextField();

        //Create a text field to display the result
        TextField resultField = new TextField();

        //Create a button to calculate the sum
        Button button = new Button("Calculate sum");

        //Create a button to reset the calculator
        Button resetButton = new Button("Reset calculator");

        //Add the text field and button to the grid pane
        gridPane.add(textField, 0, 0);
        gridPane.add(button, 0, 1);
        gridPane.add(resetButton, 0, 2);
        gridPane.add(resultField, 0, 3);

        //Set the stage
        stage.setScene(new javafx.scene.Scene(gridPane));
        stage.show();

        //Set the action for the button
        button.setOnAction(actionEvent -> {
            //Get the numbers from the text field
            String[] numbers = textField.getText().split(" ");

            //Create an array of integers
            int[] intNumbers = new int[numbers.length];

            //Convert the numbers to integers
            for (int i = 0; i < numbers.length; i++) {
                intNumbers[i] = Integer.parseInt(numbers[i]);
            }

            //Calculate the sum
            int sum = Main.sum(intNumbers, calculator);

            //Display the result
            resultField.setText(String.valueOf(sum));
        });

        //Set the action for the reset button
        resetButton.setOnAction(actionEvent -> {
            //Reset the calculator
            calculator.reset();

            //Clear the text fields
            textField.clear();
            resultField.clear();
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
