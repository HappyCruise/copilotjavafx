package calculator;
public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();

        //Sum integers 10, 7, 6
        int[] numbers = {10, 7, 6};

        System.out.println("Sum of integers: " + sum(numbers, calculator));

    }

    //Sum array of positive integers, throws exception if negative integer is found
    public static int sum(int[] numbers, Calculator calculator) {
        for (int number : numbers) {
            if (number < 0) {
                throw new IllegalArgumentException("Negative integer found");
            }
            calculator.addInteger(number);
        }
        return calculator.getValue();
    }

}