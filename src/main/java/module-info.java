module com.example.copilotfx {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.junit.jupiter.api;


    opens com.example.copilotfx to javafx.fxml;
    exports com.example.copilotfx;

//    opens calculator to javafx.graphics;
    exports calculator;
}